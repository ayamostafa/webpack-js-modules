const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const config = {
   entry:'./src/index.js',
    mode: "development",
    output: {
       path:path.resolve(__dirname,'build'),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                use:'babel-loader',
                test:/\.js$/,
            },
            {
                use:[MiniCssExtractPlugin.loader,'css-loader'],
                test:/\.css$/
            },
            {
                use:['file-loader','image-webpack-loader'],
                test:/\.(jpe?g|png|gif|svg)$/
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin()
    ]
}

module.exports = config;
